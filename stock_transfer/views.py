from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse

from utils import transfer


def index(request):
    return render(request, 'stock_transfer.html')


def get_time(request):
    if request.POST:
        warehouse = request.POST.get('warehouse_id')
        products = request.POST.get('stock_list')
        try:
            products = dict([e.strip().split(':') for e in products.split(',')])
        except:
            return HttpResponse('Invalid stock list, '
                                'e.g.: 1CEB16: 12, EF7DAB: 1')

        transfer_time = transfer.calculate_time(warehouse, products)
        if transfer_time == -1:
            return HttpResponse('Not possible to retrieve selected '
                                'stock in selected warehouse.')
        return HttpResponse('Transfer will take %s hours' % transfer_time)
    return HttpResponseRedirect(reverse('index'))
