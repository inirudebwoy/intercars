import csv
import os

BASE_DIR = os.path.dirname(__file__)


def _load_stocks():
    """
    Loads stock data from file.
    Each warehouse has stock with quantity.

    @returns (dict): data of structure {'warehouse': {'stock': quantity}}

    """
    stocks = {}
    file_path = os.path.join(BASE_DIR, 'stocks.csv')
    with open(file_path) as csvfile:
        data = csv.DictReader(csvfile)
        col1, col2, col3 = data.fieldnames
        for row in data:
            if not stocks.get(row[col1]):
                stocks[row[col1]] = {}
            stocks[row[col1]][row[col2]] = row[col3]
    return stocks


def _load_connections():
    """
    Loads connection data from file.
    Each connection consists of two warehouses and transfer time
    between them.

    @returns (list): List of lists of connections

    """
    connections = []
    file_path = os.path.join(BASE_DIR, 'connections.csv')
    with open(file_path) as csvfile:
        data = csv.DictReader(csvfile)
        col1, col2, col3 = data.fieldnames
        for row in data:
            connections.append([row[col1], row[col2], int(row[col3])])
    return connections


def calculate_time(warehouse, products):
    """
    Main function calculating transfer time.

    @arg warehouse (str): warehouse to be checked
    @arg products (dict): product ids (str) with quantities (int or str)

    @returns (int): -1 not posible, other values indicate transfer time in hours

    """
    return _calc_time_acc(warehouse, missing_stock(warehouse, products), 0, [])


def _calc_time_acc(warehouse, products, transfer_time, visited):
    """
    Function calculates transfer time by checking each connected warehouse
    until request is fulfilled unless it is not possible.

    @arg warehouse (str): warehouse to be checked
    @arg products (dict): product ids (str) with quantities (int or str)
    @arg transfer_time (int): transfer time
    @arg visited (list): list of visited warehouses

    @returns (int): -1 not posible, other values indicate transfer time in hours

    """
    neighbours = nearby_warehouses(warehouse, visited)
    if neighbours and products:
        visited.append(warehouse)
        for n in neighbours:
            products = missing_stock(n[0], products)
            transfer_time += n[1]
            if not products:
                return transfer_time
        for n in neighbours:
            return _calc_time_acc(n[0], products, transfer_time, visited)
    elif not neighbours and products:
        return -1
    else:
        return transfer_time


def missing_stock(warehouse, products):
    """
    Function to calculate what quantity of requested products
    is not satisfied by current warehouse.

    @arg warehouse (str): warehouse to be checked
    @arg products (dict): product ids (str) with quantities (int or str)

    @returns (dict): product ids with quanities not on stock
                     in current warehouse

    """
    transfer_products = {}
    warehouse_stocks = _load_stocks().get(warehouse, {})
    if not warehouse_stocks:
        return products

    for product, quantity in products.items():
        needed = int(warehouse_stocks.get(product, 0)) - int(quantity)
        if needed < 0:
            transfer_products.update({product: abs(needed)})
    return transfer_products


def nearby_warehouses(warehouse, visited):
    """
    Retrieves connected warehouses with transfer times.
    List is sorted by transfer time.

    @arg warehouse (str): warehouse which connections are neede
    @arg visited (list): list of visited warehouses

    @returns (list): list of connected warehouses along with transfer times

    """
    next_ = [row for row in _load_connections() if warehouse in row]
    [n.remove(warehouse) for n in next_]
    next_ = filter(lambda x: x[0] not in visited, next_)
    return sorted(next_, key=lambda x: x[1])
