from django.conf.urls import patterns, url
from stock_transfer import views


urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'get_time/$', views.get_time, name='get_time')
)
